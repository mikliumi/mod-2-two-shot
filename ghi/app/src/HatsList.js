import { useEffect, useState } from 'react'
import { useParams, Link } from 'react-router-dom';
import { NavLink } from 'react-router-dom';

function HatsList() {
    const [hats, setHats] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/locations/hats/');
        console.log("this is response", response);
        if (response.ok) {
            const data = await response.json();
            // console.log("datatatatatata", data)
            // console.log("data i want to see", data.hats)
            setHats(data.hats)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    const handleDelete = async (e) => {
        const url = `http://localhost:8090/locations/hats/${e.target.id}/`

        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }

        const resp = await fetch(url, fetchConfigs)
        const data = await resp.json()
        console.log("LOOOOKOKOKOKOKOKOK", data);

        setHats(hats.filter(hat => String(hat.id) !== e.target.id));
    }

    return (
        <table className="table">
            <thead>
                <tr>
                <th>Hat ID</th>
                <th>Fabric</th>
                <th>Style</th>
                <th>Color</th>
                <th>Location</th>
                <th>delete?</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                return (
                    <tr key={hat.id}>
                        <td>{ hat.id }</td>
                        <td>{ hat.fabric }</td>
                        <td>{ hat.style_name }</td>
                        <td>{ hat.color }</td>
                        <td>{ hat.location }</td>
                        <td><button onClick={handleDelete} id={hat.id} className="btn btn-danger">Adios!</button></td>
                    </tr>
                );
                })}
            </tbody>
        </table>
      );
    }



export default HatsList;
