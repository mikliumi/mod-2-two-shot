import { useEffect, useState } from 'react';

function ShoesList() {
  const [shoes, setShoes] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8080/bins/shoes/');
    
    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes)
    }
  }

  useEffect(()=>{
    getData()
  }, [])
  
  return (
    <table className="table">
      <thead>
        <tr>
          <th>Shoes ID</th>
          <th>Manufacturer</th>
          <th>Model</th>
          <th>Color</th>
          <th>Bin</th>
        </tr>
      </thead>
      <tbody>
        {shoes.map(shoes => {
          return (
            <tr key={shoes}>
              <td>{ shoes.id }</td>
              <td>{ shoes.manufacturer }</td>
              <td>{ shoes.model_name }</td>
              <td>{ shoes.color }</td>
              <td>{ shoes.bin }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default ShoesList;
