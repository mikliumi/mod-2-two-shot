import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList'
import HatsForm from './HatsForm'


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
        </Routes>
        <Routes>
          <Route path="/hats" element={<HatsList />} />
        </Routes>
        <Routes>
          <Route path="/hats/add" element={<HatsForm />} />
        </Routes>
        {/* <Routes>
          <Route path="/shoes/add" element={<ShoesForm />} />
        </Routes> */}
      </div>
    </BrowserRouter>
  );
}

export default App;
