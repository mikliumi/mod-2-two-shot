import React, {useState, useEffect } from 'react';

function HatsForm () {
    const [locations, setLocations] = useState([]);
    const [fabric, setFabric] = useState('');
    const [style_name, setStyleName] = useState('');
    const [color, setColor] = useState('');
    const [location, setLocation] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.fabric = fabric;
        data.style_name = style_name;
        data.color = color;
        data.location = location;
        console.log("this is submit: ", data);

        const hatsUrl = 'http://localhost:8090/locations/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatsUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log("potato", newHat);

            setFabric('');
            setStyleName('');
            setColor('');
            setLocation('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/'

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log("AAAAAAAAAAAAAA", data);
            setLocations(data.locations);
        }
    }

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }


    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input value={fabric} onChange={handleFabricChange} placeholder="fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={style_name} onChange={handleStyleNameChange} placeholder="style_name" required type="text" name="style_name" id="style_name" className="form-control" />
                            <label htmlFor="style_name">Hat Style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={color} onChange={handleColorChange} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="mb-3">
                            <select value={location} onChange = {handleLocationChange} required id="location" className="form-select" name="location">
                            <option value="">Choose a location</option>
                            {locations.map(location => {
                                return (
                                    <option value = {location.id} key = {location.id}>
                                    {location.closet_name}  Shelf {location.shelf_number}
                                    </option>
                                );
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}


export default HatsForm;
