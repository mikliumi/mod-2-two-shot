from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import BinVO, Shoes

# class BinVOEncoder(ModelEncoder):
#     model = BinVO
#     properties = [
#         "import_href",
#         "closet_name",
#     ]

class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "id",
    ]

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}


class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model_name",
        "color",
        # "picture_url",
        # "bin",
    ]

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):

    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoes.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder,
        )
    else:
        content = json.loads(request.body) # pluto
        print("request.method POST, printing content", content)

        try:
            bin_href = content["bin"]
            print("Printing bin_href", bin_href)
            bin = BinVO.objects.get(id=content["bin"])
            print("Printing bin", bin)
            content["bin"] = bin

        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoesDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoes(request, pk):

    if request.method == "GET":
        shoes = Shoes.objects.get(id=pk)
        return JsonResponse(
            shoes,
            encoder=ShoesDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        print("request.method PUT, printing content", content)
        try:
            if "bin" in content:
                update_href = content["bin"]
                bin = BinVO.objects.get(import_href=f"/api/bins/{update_href}/") # pluto
                content["bin"] = bin
        except Shoes.DoesNotExist:
            return JsonResponse({"message": "Shoes do not exist"})
    
    Shoes.objects.filter(id=pk).update(**content)
    shoes = Shoes.objects.get(id=pk)
    return JsonResponse(
        shoes,
        encoder=ShoesDetailEncoder,
        safe=False,
    )