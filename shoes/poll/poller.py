import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from shoes_rest, here.
from shoes_rest.models import BinVO


def get_bin():
    response = requests.get("http://wardrobe-api:8000/api/bins/")
    content = json.loads(response.content)
    for bin in content["bins"]:
        BinVO.objects.update_or_create(
            import_href=bin["href"],
            defaults={"closet_name": bin["closet_name"]},
        )

def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            get_bin()
            print("Try: Got the bin")
        except Exception as e:
            print(e, file=sys.stderr)
            print("Except: Did not get the bin")
        time.sleep(60)

# def poll():
#     while True:
#         print('Shoes poller polling for data')
#         try:
#             # Write your polling logic, here
#             response = requests.get("http://wardrobe-api:8000/api/bins/")
#             content = json.loads(response.content)
#             for bin in content["bins"]:
#                  BinVO.objects.update_or_create(
#                      import_href=bin["href"],
#                      defaults={"closet_name": bin["closet_name"]},
#                  )
#             print("Try: Is this thing on?")
#         except Exception as e:
#             print(e, file=sys.stderr)
#             print("Except: This thing is not on.")
#         time.sleep(60)

if __name__ == "__main__":
    poll()
