from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import LocationVO, Hat


# class LocationVOEncoder(ModelEncoder):
#     model = LocationVO
#     properties = [
#         "import_href",
#         "closet_name",
#     ]

class HatsListEncoder(ModelEncoder):
    model = Hat
    properties = ["fabric", "style_name", "color", "id"]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


class HatsDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        # "picture_url"
        # "location",
    ]
    def get_extra_data(self, o):
        return {"location": o.location.closet_name}
    # encoders = {
    #     "location": LocationVOEncoder(),
    # }


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats=Hat.objects.filter(location=location_vo_id)
        else:
            hats=Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,
        )
    else:
        content = json.loads(request.body)
        print("************", content)
        try:
            location_href = content['location']
            print("**********", location_href)
            location = LocationVO.objects.get(import_href = f"/api/locations/{location_href}/")
            print("*******", location)
            content["location"] = location


        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location given homie"},
                status=400,
            )
        hats = Hat.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hat(request, pk):
    if request.method == "GET":
        hat= Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        print("pizza party!", content)
        try:
            if "location" in content:
                update_href = content['location']
                print("potato potato", update_href)
                location = LocationVO.objects.get(import_href=f"/api/locations/{update_href}/")
                print("wheres the beef?!?", location)
                content["location"] = location
                print ("you want a hat? you WANT a hat?", content['location'])
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Hat Name"},
                status=400,
            )
        Hat.objects.filter(id=pk).update(**content)
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )
