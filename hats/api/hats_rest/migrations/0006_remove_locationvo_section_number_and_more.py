# Generated by Django 4.0.3 on 2023-03-03 16:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0005_locationvo_section_number_locationvo_shelf_number'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='locationvo',
            name='section_number',
        ),
        migrations.RemoveField(
            model_name='locationvo',
            name='shelf_number',
        ),
    ]
